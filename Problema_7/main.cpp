/*Programa que recibe un strin y devuelve otro sin los caracteres repetidos, lo hace atraves de iterar por todas las
 posiciones del segundo string y si no esta el i caracter lo agrega y sigue con el siguiente*/


#include <iostream>
#include <string.h>

bool buscarChar(char letra, char *palabra); //Funcion que busca la letra que recibe en el arreglo *palabra
void limpiarC(char *lista,int tam); //Limpia todas las posiciones del arreglo que recibe, recibe el puntero al arreglo
                                    // y el tamaño
using namespace std;

int main()
{
    int lon,k=0; //Inicializacion de variables
    char palabra[128]; //inicializacion del string
    cin>>palabra;

    lon=strlen(palabra); // Busca la longitud de palabra

    char palabra2[lon]; //Incia otro string con la longitud de palabra
    limpiarC(palabra2,lon); // Limpia el string 2

    for(int i=0;i<lon;i++){ //Recorre la palabra original por cada uno de sus caracteres
        if(buscarChar(palabra[i], palabra2)){ // Verifica si este caracter esta
            palabra2[k]=palabra[i]; //En caso de no estar, lo agrega en la posicion k
            k++; //suma uno a la posicion
        }
    }

    cout<<palabra2<<endl; //Lo imprime como string

    return 0;
}

bool buscarChar(char letra, char *palabra){
    for(int i=0; i<strlen(palabra);i++){  //itera por la posiciones de palabra
        if(letra==palabra[i]){ //Busca si esta la letra
            return false; //Devuelve un booleano verdadero o falso

        }
    }
    return true;
}

void limpiarC(char *lista,int tam){
    int i;
    for(i=0;i<tam; i++){ //Recorre todo el string
        *(lista+i)=0; //colocando 0 en todas sus posiciones
    }
}
